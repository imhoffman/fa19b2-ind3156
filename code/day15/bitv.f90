 module const
  implicit none
  integer, parameter :: coffee=1, breakfast=2, nap=4
 end module

 module subs
  implicit none
  contains

  subroutine cli ( arg1 )
   integer, intent(out)   :: arg1
   character (len=16)     :: arg
   integer                :: fail=0

   if ( command_argument_count() .ne. 1 ) then
     write(6,*) " You're doing it wrong."
     arg1 = -1
   end if

   call get_command_argument(1,arg,status=fail)
   if ( fail .eq. 0 ) then
    read(arg,*) arg1
   else
    arg1 = -2
   endif

   return
  end subroutine cli
 end module


 program bitv
  use const
  use subs
  implicit none
  integer  :: userv = -3

  call cli( userv )
  if ( userv .lt. 0 ) call exit( 1 )

  if ( iand( userv, coffee )    .gt. 0 ) write(6,*) 'coffee!'
  if ( iand( userv, breakfast ) .gt. 0 ) write(6,*) 'breakfast!'
  if ( iand( userv, nap )       .gt. 0 ) write(6,*) 'nap!'

  call exit( 0 )
 end program bitv

