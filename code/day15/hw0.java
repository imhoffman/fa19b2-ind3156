import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class hw0 {
 public static void main( String[] args ) throws IOException {

  String input;
  int m;
  
  System.out.format( "%n Please enter an integer: " );
  input = (new BufferedReader(new InputStreamReader(System.in))).readLine();
  m = Integer.parseInt( input );

  System.out.format( "%n  %d! = %d%n%n", m, factorial( m ) );

  System.exit( 0 );
 }


 //  this is recursive, but the final multiplication awaits the
 //  return from (n-1)! before this function can return ... so
 //  the call stack grows
 static int factorial( int n ) {
	 if ( n == 0 ) { return 1; }
	 else {          return n * factorial( n - 1 ); }
 }


}
