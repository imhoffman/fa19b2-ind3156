
def factorial ( n, accum=1 ):
    while True:
        if n == 0:
            return accum
        accum = accum * n
        n = n - 1


#  main program

print( "\n Please enter an integer:" )

m = int( input() )

print( "\n  %d! = %d\n" % ( m, factorial( m ) ) )


