import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class hw1 {
 public static void main( String[] args ) throws IOException {

  String input;
  int m;
  
  System.out.format( "%n Please enter an integer: " );
  input = (new BufferedReader(new InputStreamReader(System.in))).readLine();
  m = Integer.parseInt( input );

  System.out.format( "%n  %d! = %d%n%n", m, factorial( m, 1 ) );

  System.exit( 0 );
 }


 //  this is recursive with a return that can be accomplished immeadiately
 //  because everything that needs to be known can be known right now; the
 //  call stack does not grow because only one function is executed at a time;
 //  the value of `accum` grows via the successive completion of single
 //  functions
 static int factorial( int n, int accum ) {
	 if ( n == 0 ) { return accum; }
	 else {          return factorial( n - 1, n * accum ); }
 }


}
