#include<stdio.h>
#include<stdlib.h>

int main(void) {
 unsigned int bitv=0;
 unsigned int coffee=2, breakfast=3, nap=4;
 
 printf("\n         original bitv: %2u\n\n", bitv);

 bitv |= coffee;

 printf("\n  bitv after |= coffee: %2u\n\n", bitv);

 bitv |= nap;

 printf("\n     bitv after |= nap: %2u\n\n", bitv);

 bitv &= ~coffee;

 printf("\n bitv after &= ~coffee: %2u\n\n", bitv);

 return 0;
}
