
def factorial ( n, accum, g ):
    if n == 0:
        return accum
    else:
        return g( n - 1, accum * n, g )


#  main program

print( "\n Please enter an integer:" )

m = int( input() )

print( "\n  %d! = %d\n" % ( m, factorial( m, 1, factorial ) ) )


