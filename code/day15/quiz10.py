
def factorial ( n, accum ):
    if n == 0:
        return accum
    else:
        return factorial( n - 1, accum * n )


#  main program

print( "\n Please enter an integer:" )

m = int( input() )

print( "\n  %d! = %d\n" % ( m, factorial( m, 1 ) ) )


