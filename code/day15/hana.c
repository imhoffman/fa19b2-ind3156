#include<stdio.h>
#include<stdlib.h>

int main( int argc, char* argv[] ) {
 
 int m;
 unsigned int coffee, breakfast, nap, oj;
 
 coffee    = 0b0001;      //    coffee = 1;  
 breakfast = 0b0010;      //    breakfast = 2;  
 nap       = 0b0100;
 oj        = 0b1000;
 
 m = atoi( argv[1] );     // no error checking whatsoever

 if ( m & coffee )    { printf( "\n coffee.\n\n" ); }
 if ( m & breakfast ) { printf( "\n breakfast.\n\n" ); }
 if ( m & nap )       { printf( "\n nap.\n\n" ); }
 if ( m & oj )        { printf( "\n oj.\n\n" ); }
 
 return 0;
}
