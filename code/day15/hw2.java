import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class hw2 {
 public static void main( String[] args ) throws IOException {

  String input;
  int m;
  
  System.out.format( "%n Please enter an integer: " );
  input = (new BufferedReader(new InputStreamReader(System.in))).readLine();
  m = Integer.parseInt( input );

  System.out.format( "%n  %d! = %d%n%n", m, factorial( m, 1 ) );

  System.exit( 0 );
 }


 //  this is not recursive because there are no function calls within the 
 //  body; nevertheless, it accomplishes the same arithmetic end; obviously,
 //  it does not grow the call stack because nothing is called
 static int factorial( int n, int accum ) {
	 while ( true ) {
		if ( n == 0 ) { return accum; }
	 	accum = n * accum;
	 	n = n - 1;
	 }	
 }


}
