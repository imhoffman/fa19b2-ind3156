#include<stdio.h>
#include<math.h>

int main(void) {
 int j;
 float x;
// double x;
// __float128 x;

    x = 1.0F;
    j = 0;
    while ( x+1.0F != x ) {
//     printf(" j=%3d, 10s=%5.2f, x=%.1Lf\n", j, (float)j*log10(2.0), (long double)x);
     printf(" j=%3d, 10s=%5.2f, x=%.1f\n", j, (float)j*log10(2.0), x);
     j = j + 1;
//     x = x * 2.0;
     x = x + x;  // doubling x this way removes the dependence on the 2.0 literal
    }

 return 0;
}
