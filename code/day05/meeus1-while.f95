  program meeus1
  implicit none

  integer (kind=4)  :: j
  real (kind=16)    :: x
  character (len=*), parameter :: fmt1 = '(A,I3,A,F6.2,A,F38.1)'

  x = 2.
  j = 1
  do while ( x+1. .ne. x )
   write(*,fmt1) ' j=', j, ', 10s=', j*log10(2.), ',  x=', x
   j = j + 1
   x = x * 2.
  end do

  stop
  end program meeus1
