#include<stdio.h>

int main(void) {
 int i;

// float x;
 double x;
// long double x;

// x = 1.0F/3.0F;
 x = 1.0/3.0;
// x = 1.0L/3.0L;

 for ( i = 0 ; i < 38 ; i++ ) {

  printf(" i=%3d,  x=%+29.23e\n", i, x);
//  printf(" i=%3d,  x=%+29.23Le\n", i, x);

//  x = (9.0F*x + 1.0F)*x - 1.0F;
  x = (9.0*x + 1.0)*x - 1.0;
//  x = (9.0L*x + 1.0L)*x - 1.0L;

 }
 return 0;
}
