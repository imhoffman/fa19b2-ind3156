# https://docs.python.org/3/library/stdtypes.html#numeric-types-int-float-complex
# https://docs.python.org/3/library/sys.html#sys.int_info
import sys

x = 1
for j in range(2,129):
 x = x * 2 
 print(j, x)

print("\n the bits per digit on this system is %d\n" % sys.int_info.bits_per_digit )
print(" the size of a digit on this system is %d\n" % sys.int_info.sizeof_digit )
print(" the largest storable integer on this system is reported to be %d\n" % sys.maxsize )

print(" x requires %d bytes---or perhaps %d bytes---to be stored\n" % ( x.__sizeof__(), sys.getsizeof(x) ) )

