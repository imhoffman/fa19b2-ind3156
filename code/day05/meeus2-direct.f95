 program meeus2
  implicit none

  integer         :: i
  real (kind= 8)  :: x
  !real (kind=16)  :: x

  x = 1.0D0 / 3.0D0
  !x = 1.0Q0 / 3.0Q0
  do i = 0, 60
   x = ( 9.0D0*x + 1.0D0 )*x - 1.0D0
   !x = ( 9.0Q0*x + 1.0Q0 )*x - 1.0Q0
   write(6,*) i, x
  end do

  stop
 end program meeus2
