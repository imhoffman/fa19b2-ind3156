# https://www.numpy.org/devdocs/user/basics.types.html#extended-precision

x = 1.0/3.0
for i in range(0,33):
    print(" i=%3u,  x=%+.23g" % (i,x))
    x = (9.0*x + 1.0)*x - 1.0
