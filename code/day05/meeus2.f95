 program meeus2
! use iso_fortran_env

 integer :: i
! real (kind=real128) :: x
 real (kind= 8) :: x

! x = 1._16/3._16
 x = 1._8/3._8
 do i = 0, 56
   write(6,'(A,I3,A,SP,G0.36)') '  i=', i, ',  x= ', x
!   x = (9._16*x + 1._16)*x - 1._16
   x = (9._8*x + 1._8)*x - 1._8
 end do

 stop
 end program meeus2
