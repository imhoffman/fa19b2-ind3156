# https://docs.python.org/3/library/stdtypes.html#numeric-types-int-float-complex
# https://docs.python.org/3/library/sys.html#sys.float_info
import sys

j=0
x = 1.0
while ( x != x+1.0 ):
 x = x * 2.0 
 j = j + 1
 print(j, x)

print("\n you should be able to trust %d significant digits in a float on this system\n" % sys.float_info.dig)
print(" the float requires %d bytes---or perhaps %d bytes---to be stored on this system\n" % ( x.__sizeof__(), sys.getsizeof(x) ) )
