  program meeus1
  implicit none

  integer (kind=4)  :: j
  real (kind=16)    :: x

      x = 1.
      j = 0
30    x = 2.*x
      if ( x+1. .ne. x ) goto 60
50    goto 80
60    j = j + 1
      write(*,65) ' j=', j, ', 10s=', j*log10(2.), ',  x=', x
65    format(A,I3,A,F6.2,A,F38.1)
70    goto 30
80    continue

  stop
  end program meeus1
