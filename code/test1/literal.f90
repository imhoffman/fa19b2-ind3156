 program literal
  implicit none

  real (kind=4) :: x

  x = z'3e200000'
  write(6,'(F14.6)') x

  x = z'be200000'
  write(6,'(F14.6)') x

  stop
 end program literal
