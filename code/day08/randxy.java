import java.util.Random;

public class randxy {
 public static void main(String[] args) {

  int i, nTotal, nCounted=0;
  double x, y, xmin, xmax, ymin, ymax;
  Random r;

  xmin = -1.0;
  xmax = +1.0;
  ymin = -5.0;
  ymax = +5.0;

  r = new Random( System.currentTimeMillis() );
  //r = new Random( 42 );

  nTotal = 8192;
  //nTotal = Integer.parseInt( args[0] );

  for ( i=0; i < nTotal; i++ ) {

   x = (xmax-xmin)*r.nextDouble()+xmin;
   y = (ymax-ymin)*r.nextDouble()+ymin;

   if ( x < 0.0  &&  y < 0.0 ) { nCounted = nCounted + 1; }

  }

  System.out.format("%n fraction = %+18.16f%n%n", (double)nCounted/nTotal );

 }
}
