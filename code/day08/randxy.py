import random, os, sys

random.seed( os.urandom( 128 ) )

xmin = -1.0
xmax = +1.0
ymin = -5.0
ymax = +5.0

if ( len(sys.argv) == 2 ):
    ntotal = int( sys.argv[1] )
else:
    ntotal = 8192

ncount = 0
for i in range(ntotal):
    x = (xmax-xmin)*random.random() + xmin
    y = (ymax-ymin)*random.random() + ymin
    if ( x < 0.0  and  y < 0.0 ):
        ncount = ncount + 1

print( "\n fraction = %+18.16f\n" % float(ncount/ntotal) )
