import random
import os

#random.seed(42)
#random.seed()                     # leaving it blank seeds it for you ...
random.seed( os.urandom( 128 ) )  # `128` is a number of bytes, not a seed value

xmin = -1.0
xmax = +1.0
ymin = -5.0
ymax = +5.0
for i in range(3):
    x = (xmax-xmin)*random.random()+xmin
    y = (ymax-ymin)*random.random()+ymin
    print("\n    x = %+18.16f" % x)
    print("    y = %+18.16f" % y)
print("")
