 program rand0
  !use ifport      ! uncomment for ifort
  implicit none

  integer       ::  i
  real (kind=8) ::  x, y, xmin, xmax, ymin, ymax

  xmin = -1.0D0
  xmax = +1.0D0
  ymin = -5.0D0
  ymax = +5.0D0

  call srand(42)

!  x = rand()
  write(6,*) ''
  do i = 1, 3
    x = (xmax-xmin)*rand()+xmin
    y = (ymax-ymin)*rand()+ymin
    write(6,'(A12,SPF19.16)') '        x = ', x
    write(6,'(A12,SPF19.16)') '        y = ', y
    write(6,*) ''
  end do

  stop
 end program rand0
