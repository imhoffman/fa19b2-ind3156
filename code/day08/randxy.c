#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#include<math.h>
#include<sys/types.h>
#include<unistd.h>
 
int main ( int argc, char* argv[] ) {
 int i, nTotal, nCounter=0;
 double x, y;
 double xmin, xmax, ymin, ymax;
 pid_t getpid(void);
 
 xmin = -1.0;
 xmax = +1.0;
 ymin = -5.0;
 ymax = +5.0;
 
 srand(time(NULL) + getpid());

 if ( argc == 2 ) {
   nTotal = atoi( argv[1] );
 } else {
   nTotal = 8192;
 }
 
 for ( i=0; i < nTotal; i++ ) {

  x =(double) (xmax-xmin)*(rand() / (RAND_MAX + 1.0))+xmin;
  y =(double) (ymax-ymin)*(rand() / (RAND_MAX + 1.0))+ymin;

  if ( x < 0.0  &&  y < 0.0 ) { nCounter = nCounter + 1; }

 }

 printf( "\n fraction = %+18.16f\n\n", (double) nCounter/nTotal  );
 
 return 0;
}
