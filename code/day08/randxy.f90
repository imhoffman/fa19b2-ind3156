 module subs
  implicit none
  contains

  subroutine seeder(n)
   integer :: i, n, seed(n)
   open (unit=10, file='/dev/urandom', access='stream',  &
              form='unformatted', status='old')
   do i = 1, n
     read(10,err=1300) seed(i)
   end do
   close(10)
   call random_seed(put=seed)
   1300  continue
  end subroutine seeder
 end module subs

 program randxy
  use subs
  implicit none

  integer            :: i, sdsz, ntotal, ncount=0
  character (len=80) :: arg
  real ( kind=8)     :: x, y, xmin, xmax, ymin, ymax

  xmin = -1.0D0
  xmax = +1.0D0
  ymin = -5.0D0
  ymax = +5.0D0

  call random_seed(size=sdsz)
  call seeder(sdsz)

  if ( command_argument_count() .eq. 1 ) then
    call get_command_argument(1,arg)
    read(arg,*) ntotal
  else
    ntotal = 8192
  endif

  write(6,*) ''
  do i = 1, ntotal
    call random_number(x)
    call random_number(y)
    x = (xmax-xmin)*x + xmin
    y = (ymax-ymin)*y + ymin

    if ( x .lt. 0.0D0  .and.  y .lt. 0.0D0 ) ncount = ncount + 1

  end do

  write(6,'(A12,SPF19.16)') ' fraction = ', dble(ncount)/dble(ntotal)
  write(6,*) ''

  stop
 end program randxy
