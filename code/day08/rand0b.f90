 module subs
  implicit none
  contains

  subroutine seeder(n)
   integer :: i, n, seed(n)
   open (unit=10, file='/dev/urandom', access='stream',  &
              form='unformatted', status='old')
   do i = 1, n
     read(10,err=1300) seed(i)
   end do
   close(10)
   call random_seed(put=seed)
   1300  continue
  end subroutine seeder
 end module subs

 program rand0b
  use subs
  !use ifport
  implicit none

  integer           :: i, sdsz
  real ( kind=8)    :: x, y, xmin, xmax, ymin, ymax

  xmin = -1.0D0
  xmax = +1.0D0
  ymin = -5.0D0
  ymax = +5.0D0

  call random_seed(size=sdsz)
  call seeder(sdsz)

  write(6,*) ''
  do i = 1, 3
    call random_number(x)
    call random_number(y)
    x = (xmax-xmin)*x+xmin
    y = (ymax-ymin)*y+ymin
    write(6,'(A12,SPF19.16)') '        x = ', x
    write(6,'(A12,SPF19.16)') '        y = ', y
    write(6,*) ''
  end do

  stop
 end program rand0b
