import java.util.Random;

public class rand0 {
 public static void main(String[] args) {

  int i;
  double x, y, xmin, xmax, ymin, ymax;
  Random r;

  xmin = -1.0;
  xmax = +1.0;
  ymin = -5.0;
  ymax = +5.0;

  r = new Random(42);

  for ( i=0; i < 3; i++ ) {
   x = (xmax-xmin)*r.nextDouble()+xmin;
   y = (ymax-ymin)*r.nextDouble()+ymin;
   System.out.format("%n        x = %+18.16f%n", x);
   System.out.format("        y = %+18.16f%n", y);
  }
  System.out.format("%n");
 }
}
