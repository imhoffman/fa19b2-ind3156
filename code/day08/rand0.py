import random

random.seed(42)

xmin = -1.0
xmax = +1.0
ymin = -5.0
ymax = +5.0
for i in range(3):
    x = (xmax-xmin)*random.random()+xmin
    y = (ymax-ymin)*random.random()+ymin
    print("\n    x = %+18.16f" % x)
    print("    y = %+18.16f" % y)
print("")
