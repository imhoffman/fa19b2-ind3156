#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#include<math.h>
#include<sys/types.h>
#include<unistd.h>
 
int main(void) {
 int i;
 double x, y;
 double xmin, xmax, ymin, ymax;
 pid_t getpid(void);
 
 xmin = -1.0;
 xmax = +1.0;
 ymin = -5.0;
 ymax = +5.0;
 
 srand(time(NULL) + getpid());
 
 for ( i=0; i < 3; i++ ) {
  x =(double) (xmax-xmin)*(rand() / (RAND_MAX + 1.0))+xmin;
  y =(double) (ymax-ymin)*(rand() / (RAND_MAX + 1.0))+ymin;
  printf("\n        x = %+18.16f\n",x);
  printf("        y = %+18.16f\n",y);
 }
 
 return 0;
}
