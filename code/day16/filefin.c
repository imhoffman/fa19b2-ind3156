#include<stdio.h>

#define BUFSIZE         80

int main(void) {
 FILE* f;
 char buffer[BUFSIZE]= {'\0'};

 int i = 0;

 f = fopen("input.txt","r");
 while ( fgets(buffer, BUFSIZE, f) != NULL ) {
  printf( " %s", buffer );
  i++;
 }
 fclose(f);

 printf( "\n Read %d lines.\n\n", i );

 return 0;
}
