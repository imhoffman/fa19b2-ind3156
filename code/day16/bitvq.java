import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Random;

public class bitvq {

 static String[] questions = { "Do you own an Intel laptop?",
                               "Do you own an Intel desktop?",
                               "Do you own a Raspberry Pi?",
                               "Do you own an Atomic Pi?" };


 static int numQuestions = 4;

 //  command-line interface
 static int cli ( ) throws IOException {
  String input;
  int q = 0, bitv = 0;

  repl:
  while ( q < numQuestions ) {

    System.out.format( "\n %s (y/n): ", questions[q] );
    input = (new BufferedReader(new InputStreamReader(System.in))).readLine();

    //  check for blank or useless inputs
    if ( input.isEmpty() ) { continue repl; }
    if ( input.charAt(0) != 'y'  &&  input.charAt(0) != 'n' ) { continue repl; }

    if ( input.charAt(0) == 'y' ) {
        bitv |= ( 1 << q );
	q = q + 1;
    } else {
	q = q + 1;
    }
  }
  return bitv;
 }

 //
 //  main program
 //
 public static void main(String[] args) throws IOException {
  int e = 99;

  System.out.format( "\n Bit vector questionnaire, Java Edition.\n" );
  e = cli();

  System.out.format( "\n Exiting with code %d.\n\n", e );
  System.exit( e );
 }

}
