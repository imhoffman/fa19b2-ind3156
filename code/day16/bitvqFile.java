import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Formatter;
import java.io.FileWriter;
import java.io.File;

public class bitvqFile {

 static String[] questions = { "Do you own an Intel laptop?",
                               "Do you own an Intel desktop?",
                               "Do you own a Raspberry Pi?",
                               "Do you own an Atomic Pi?" };


 static int numQuestions = 4;

 //  command-line interface
 static int cli ( ) throws IOException {
  String input;
  int q = 0, bitv = 0;

  repl:
  while ( q < numQuestions ) {

    System.out.format( "\n %s (y/n): ", questions[q] );
    input = (new BufferedReader(new InputStreamReader(System.in))).readLine();

    //  check for blank or useless inputs
    if ( input.isEmpty() ) { continue repl; }
    if ( input.charAt(0) != 'y'  &&  input.charAt(0) != 'n' ) { continue repl; }

    if ( input.charAt(0) == 'y' ) {
        bitv |= ( 1 << q );
	q = q + 1;
    } else {
	q = q + 1;
    }
  }
  return bitv;
 }


 static void writer ( String[] argv, int output ) throws IOException {
   File filename = new File( argv[0] );
   try ( Formatter fileout = new Formatter(new FileWriter( filename )); ) {
     System.out.format( "\n Writing %d to %s.\n\n", output, filename );
     fileout.format( "%d\n", output );
   }
   return;
 }

 //
 //  main program
 //
 public static void main(String[] args) throws IOException {

  System.out.format( "\n Bit vector questionnaire, file writer, Java Edition.\n" );
  writer( args, cli() );

  System.exit( 0 );
 }

}
