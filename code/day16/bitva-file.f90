 module const
  implicit none
  integer, parameter                         :: nques = 4
  character(len=26), dimension(4), parameter :: answers =  (/ &
          "You own an Intel laptop. ",  &
          "You own an Intel desktop.", &
          "You own a Raspberry Pi.  ",   &
          "You own an Atomic Pi.    " /)
 end module const

 module subs
  use const
  implicit none
  contains

  subroutine cli ( bitv )
   integer, intent(in) :: bitv
   integer             :: q

   !!  https://gcc.gnu.org/onlinedocs/gfortran/ISHFT.html
   do q = 1, nques
     if ( iand( bitv, ishft( 1, q-1 ) ) .gt. 0 ) write(6,*) answers(q)
   end do

   write(6,*) ''
   return
  end subroutine cli

  function reader ( )
    integer            :: reader
    character (len=80) :: arg

    ! no error checking
    call get_command_argument(1,arg)
    open(10, file=trim(arg) )
    read(10,*) reader
    close(10)

    return
  end function reader
 end module subs


 program bitva
  use const
  use subs
  implicit none

  write(6,'(/,A,/)') ' Bit vector interpreter, file reader, Fortran Edition.'
  call cli( reader() )

  call exit( 0 )
 end program bitva

