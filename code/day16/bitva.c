#include<stdio.h>
#include<stdlib.h>

const char* answers[] = { "You own an Intel laptop.",
                          "You own an Intel desktop.",
                          "You own a Raspberry Pi.",
                          "You own an Atomic Pi." };

const int num_answers = 4;


void cli ( char* v[] ) {
 int q = 0, bitv = 0;

 bitv = atoi( v[1] );      // no error handling of user input

 while( q < num_answers ) {

   if ( bitv & ( 1 << q ) ) {
       printf( " %s\n", answers[q] );
       q++;
   } else {
       q++;
   }
 }

 printf( "\n" );
 return;
}

//
//  main program
//
int main ( int argc, char* argv[] ) {

 printf( "\n Bit vector interpreter, C Edition.\n\n" );
 cli( argv );

 return 0;
}
