 module const
  implicit none
  integer, parameter                         :: nques = 4
  character(len=28), dimension(4), parameter :: questions =  (/ &
          "Do you own an Intel laptop? ",  &
          "Do you own an Intel desktop?", &
          "Do you own a Raspberry Pi?  ",   &
          "Do you own an Atomic Pi?    " /)
 end module

 module subs
  use const
  implicit none
  contains

  function cli ( )
   integer           :: cli
   character(len=80) :: arg
   character         :: s
   integer           :: q = 1

   !!  https://gcc.gnu.org/onlinedocs/gfortran/IBSET.html
   cli = 0
   do while ( q .le. nques )
     10 write(6,'(/,A,A)',advance='no') trim(questions(q)), " (y/n): "
     read(*,*) arg
     s = arg(1:1)
     if ( s .ne. 'y'  .and.  s .ne. 'n' ) then
       goto 10
     else if ( s .eq. 'y' ) then
       cli = ibset( cli, q-1 )
       q = q + 1
     else
       q = q + 1
     end if
   end do
  end function cli
 end module


 program bitvq
  use const
  use subs
  implicit none
  integer  :: e = 99 

  write(6,'(/,A,/)') ' Bit vector questionnaire, Fortran Edition.'
  e = cli( )

  write(6,'(/,A,I0,A,/)') ' Exiting with code ', e, '.'
  call exit( e )
 end program bitvq

