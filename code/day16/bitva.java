
public class bitva {

 static String[] answers = { "You own an Intel laptop.",
                             "You own an Intel desktop.",
                             "You own a Raspberry Pi.",
                             "You own an Atomic Pi." };


 static int numAnswers = 4;

 //  command-line interface
 static void cli ( String[] v ) {
  int q = 0, bitv = 0;

  bitv = Integer.parseInt( v[0] );    // no error handling of user input

  while ( q < numAnswers ) {

    if ( ( bitv & ( 1 << q ) ) > 0 ) {
      System.out.format( " %s\n", answers[q] );
      q++;
    } else {
      q++;
    }

  }

  System.out.format( "\n" );
  return;
 }

 //
 //  main program
 //
 public static void main( String[] args ) {

  System.out.format( "\n Bit vector interpreter, Java Edition.\n\n" );
  cli( args );

  System.exit( 0 );
 }

}
