 module const
  implicit none
  integer, parameter                         :: nques = 4
  character(len=26), dimension(4), parameter :: answers =  (/ &
          "You own an Intel laptop. ",  &
          "You own an Intel desktop.", &
          "You own a Raspberry Pi.  ",   &
          "You own an Atomic Pi.    " /)
 end module const

 module subs
  use const
  implicit none
  contains

  subroutine cli ( e )
   integer, intent(inout) :: e
   character (len=16)     :: arg
   integer                :: q, fail=0, bitv=0

   if ( command_argument_count() .ne. 1 ) then
     write(6,*) " You're doing it wrong."
     e = 1
     goto 300
   end if

   !!  https://gcc.gnu.org/onlinedocs/gfortran/ISHFT.html
   call get_command_argument(1,arg,status=fail)
   if ( fail .eq. 0 ) then
    read(arg,*,err=200) bitv
    do q = 1, nques
      if ( iand( bitv, ishft( 1, q-1 ) ) .gt. 0 ) write(6,*) answers(q)
    end do
    e = 0
   else
    e = 2
   endif

   100 goto 300
   200 e = 3
       write(6,*) ' parsing error'
   300 continue
   write(6,*) ''
   return
  end subroutine cli
 end module subs


 program bitva
  use const
  use subs
  implicit none
  integer  :: e = 4

  write(6,'(/,A,/)') ' Bit vector interpreter, Fortran Edition.'
  call cli( e )

  call exit( e )
 end program bitva

