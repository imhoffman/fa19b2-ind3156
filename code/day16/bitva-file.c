#include<stdio.h>
#include<stdlib.h>

const char* answers[] = { "You own an Intel laptop.",
                          "You own an Intel desktop.",
                          "You own a Raspberry Pi.",
                          "You own an Atomic Pi." };

const int num_answers = 4;


void cli ( int bitv ) {
 int q = 0;

 while( q < num_answers ) {

   if ( bitv & ( 1 << q ) ) {
       printf( " %s\n", answers[q] );
       q++;
   } else {
       q++;
   }
 }

 printf( "\n" );
 return;
}


int reader ( char* v[] ) {
 int input = 0;

 //  no handling of user errors
 FILE* f = fopen( v[1], "r" );
 fscanf( f, "%d\n", &input );
 fclose(f);

 return input;
}

//
//  main program
//
int main ( int argc, char* argv[] ) {

 printf( "\n Bit vector interpreter, file reader, C Edition.\n\n" );
 cli( reader( argv ) );

 return 0;
}
