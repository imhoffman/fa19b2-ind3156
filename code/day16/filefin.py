
file_contents = []

with open( "input.txt" ) as f:
    while True:
        line = f.readline()
        if not line: break
        file_contents.append( line )
#f.close        # not needed, `with` handles closing

[ print( s, end='' ) for s in file_contents ]

print( "\n Read %d lines.\n" % len( file_contents ) )

