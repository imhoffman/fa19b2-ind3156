#include<stdio.h>
#include<string.h>

const char* questions[] = { "Do you own an Intel laptop?",
                            "Do you own an Intel desktop?",
                            "Do you own a Raspberry Pi?",
                            "Do you own an Atomic Pi?" };

const int num_questions = 4;


int cli ( void ) {
 int n = 80, q = 0, bitv = 0;
 char arg[n], s='\0';

 while( q < num_questions ) {
   begin: printf( "\n" );
   printf( " %s (y/n):  ", questions[q] );
   fgets( arg, n, stdin );
   strncpy( &s, arg, 1 );

   if ( s != 'y'  &&  s != 'n' ) {
       goto begin; 
   } else if ( s == 'y' ) {
       bitv |= ( 1 << q );     // bit shift ... same as 2**q
       q++;
   } else {
       q++;
   }
 }

 return bitv;
}

//
//  main program
//
int main ( void ) {

 int e = 99;

 printf( "\n Bit vector questionnaire, C Edition.\n" );
 e = cli( );

 printf( "\n Exiting with code %d.\n\n", e ); 
 return e;
}
