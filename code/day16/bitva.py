import sys

answers = [ "Intel laptop", "Intel desktop", "Raspberry Pi", "Atomic Pi" ]

m = int( sys.argv[1] )

print( "\n Bit vector interpreter, Python Edition." )
print( "\n You own:" )

[ print( answers[i] ) for i in range( len(answers) ) if m & 2**i ]

print( "" )

