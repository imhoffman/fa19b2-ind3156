#include<stdio.h>
#include<string.h>

const char* questions[] = { "Do you own an Intel laptop?",
                            "Do you own an Intel desktop?",
                            "Do you own a Raspberry Pi?",
                            "Do you own an Atomic Pi?" };

const int num_questions = 4;


int cli ( void ) {
 int n = 80, q = 0, bitv = 0;
 char arg[n], s='\0';

 while( q < num_questions ) {
   begin: printf( "\n" );
   printf( " %s (y/n):  ", questions[q] );
   fgets( arg, n, stdin );
   strncpy( &s, arg, 1 );

   if ( s != 'y'  &&  s != 'n' ) {
       goto begin; 
   } else if ( s == 'y' ) {
       bitv |= ( 1 << q );     // bit shift ... same as 2**q
       q++;
   } else {
       q++;
   }
 }

 return bitv;
}


void writer ( int out, char *name ) {
  FILE* f = fopen( name, "w" );
  printf( "\n Writing %d to %s.\n\n", out, name );
  fprintf( f, "%d\n", out );
  fclose(f);
  return;
} 

//
//  main program
//
int main ( int argc, char *argv[] ) {

 char *filename = argv[1];

 printf( "\n Bit vector questionnaire, file writer, C Edition.\n" );

 writer( cli(), filename );

 return 0;
}
