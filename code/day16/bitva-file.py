import sys

answers = [ "Intel laptop", "Intel desktop", "Raspberry Pi", "Atomic Pi" ]

with open( sys.argv[1] ) as filename:
    bitv = int( filename.readline() )

print( "\n Bit vector interpreter, file reader, Python Edition." )
print( "\n You own:" )

[ print( answers[i] ) for i in range( len(answers) ) if bitv & 2**i ]

print( "" )

