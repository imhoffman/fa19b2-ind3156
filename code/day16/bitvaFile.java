import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class bitvaFile {

 static String[] answers = { "You own an Intel laptop.",
                             "You own an Intel desktop.",
                             "You own a Raspberry Pi.",
                             "You own an Atomic Pi." };


 static int numAnswers = 4;

 //  command-line interface
 static void cli ( int bitv ) {
  int q = 0;

  while ( q < numAnswers ) {

    if ( ( bitv & ( 1 << q ) ) > 0 ) {
      System.out.format( " %s\n", answers[q] );
      q++;
    } else {
      q++;
    }

  }

  System.out.format( "\n" );
  return;
 }


 //  https://stackoverflow.com/questions/5868369/how-to-read-a-large-text-file-line-by-line-using-java
 static int reader ( String[] v ) throws IOException {
  int bitv;
  try (BufferedReader br = new BufferedReader(new FileReader( v[0] ))) {
    String line = br.readLine();
    bitv = Integer.parseInt( line );
  }
  return bitv;
}

 //
 //  main program
 //
 public static void main( String[] args ) throws IOException {

  System.out.format( "\n Bit vector interpreter, file reader, Java Edition.\n\n" );
  cli( reader( args ) );

  System.exit( 0 );
 }

}
