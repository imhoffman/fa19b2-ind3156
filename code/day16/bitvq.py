import sys

questions = [ "Do you own an Intel laptop?",
              "Do you own an Intel desktop?",
              "Do you own a Raspberry Pi?",
              "Do you own an Atomic Pi?" ]

num_questions = len( questions )

class label(Exception): pass     # try/catch a la rps
def cli ( ):
    q = 0
    bitv = 0
    while ( q < num_questions ):
        print( "\n %s (y/n): " % questions[q], end='' )
        s = input()
        try:
            if not s or ( s[0] != 'y' and s[0] != 'n' ):     # lazy eval
                if not s: raise label()
            elif s[0] == 'y':
                bitv |= 2**q
                q = q + 1
            else:
                q = q + 1
        except label:
            pass
    return bitv
            

##  main program

print( "\n Bit vector questionnaire, Python Edition." )
e = cli()

print( "\n Exiting with code %d.\n" % e )
sys.exit( e )

