import sys

questions = [ "Do you own an Intel laptop?",
              "Do you own an Intel desktop?",
              "Do you own a Raspberry Pi?",
              "Do you own an Atomic Pi?" ]

num_questions = len( questions )

class label(Exception): pass     # try/catch a la rps
def cli ( ):
    q = 0
    bitv = 0
    while ( q < num_questions ):
        print( "\n %s (y/n): " % questions[q], end='' )
        s = input()
        try:
            if not s or ( s[0] != 'y' and s[0] != 'n' ):     # lazy eval
                if not s: raise label()
            elif s[0] == 'y':
                bitv |= 2**q
                q = q + 1
            else:
                q = q + 1
        except label:
            pass
    return bitv

def writer ( n , filename ):
    print( "\n Writing %d to %s.\n" % ( n, filename ) )
    with open( filename, "w" ) as f:
        f.write( "%d\n" % n )
            

##  main program

print( "\n Bit vector questionnaire, file writer, Python Edition." )

writer( cli(), sys.argv[1] )

sys.exit( 0 )

