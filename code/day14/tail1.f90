!!
!!  complement to `tail1.c`
 module subs
  implicit none
  contains
  subroutine print_stack_address ( n )
          integer, intent(in) :: n
          integer :: dummy

          write(6,'(A,I0,A,Z0)') ' n: ', n, ', representative address: 0x', loc(dummy)

          return
  end subroutine print_stack_address

  function factorial ( n, accum, g ) result ( answer )    ! not declared recursive
          integer, intent(in) :: n, accum
          integer             :: answer
          integer, external   :: g

          if ( n .le. 0 ) then
                  answer = accum
          else
                  call print_stack_address( n )
                  answer = g( n-1, n*accum, g )
          end if

          return
  end function factorial
 end module subs

 program main
  use subs
  implicit none
  integer :: m, acc

  m = 5
  acc =  factorial( m, 1, factorial )

  write(6,'(/,A,I0,A,/,I0,/)') " factorial of ", m, ": ", acc

  stop
 end program main

