#include<stdio.h>

void print_stack_address ( int n ) {
	int dummy;
	printf( " n: %d, representative address: %p\n", n, &dummy );
	return;
}


int factorial ( int n ) {
	if ( n == 0 ) { return 1; }
	else {
		print_stack_address( n );
		return n * factorial( n-1 );
	}
}


int main ( void ) {

	int m = 5 ;

	printf( "\n factorial of %d: %d\n\n", m, factorial( m ) );

	return 0;
}
