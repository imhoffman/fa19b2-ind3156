#include<stdio.h>

//  https://gcc.gnu.org/onlinedocs/gcc/Local-Register-Variables.html
//   use %rsp since its an output (?)
void print_stack_address ( int n ) {
	register int rsp asm("rsp");
	printf( " n: %d, stack frame address: %x\n", n, rsp );
	return;
}


int factorial ( int n ) {
	if ( n == 0 ) { return 1; }
	else {
		print_stack_address( n );
		return n * factorial( n-1 );
	}
}


int main ( void ) {

	int m = 5 ;

	printf( "\n factorial of %d: %d\n\n", m, factorial( m ) );

	return 0;
}
