#include<stdio.h>

//  `&dummy` should be cast to `void *`
//   https://stackoverflow.com/questions/30354097/how-to-printf-a-memory-address-in-c
void print_stack_address ( int n ) {
	int dummy;
	printf( " n: %d, representative address: %p\n", n, &dummy );
	return;
}


int factorial ( int n, int accum ) {

	if ( n == 0 ) { return accum; }
	else {
		print_stack_address( n );
		return factorial( n - 1, n * accum );
	}
}


int main ( void ) {

	int m = 5 ;

	printf( "\n factorial of %d: %d\n\n", m, factorial( m, 1 ) );

	return 0;
}
