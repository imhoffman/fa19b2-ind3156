!!
!!  complement to `tail.c`
!!  $ gfortran -fcray-pointer tail.f90
 module subs
  implicit none
  contains
  subroutine print_stack_address ( n )
          integer, intent(in) :: n
          integer :: dummy

          write(6,'(A,I0,A,Z0)') ' n: ', n, ', representative address: 0x', loc(dummy)

          return
  end subroutine print_stack_address

  function factorial ( n, accum ) result ( answer )
          integer, intent(inout) :: n, accum
          integer                :: answer
          10 continue
          if ( n .le. 0 ) then
                  answer = accum
                  goto 20
          else
                  call print_stack_address( n )
                  accum = accum * n
                  n = n - 1
                  goto 10
          end if
          20 continue
          return
  end function factorial
 end module subs

 program main
  use subs
  implicit none
  integer :: m, accum

  accum = 1    !! must be a variable with an address for passing to factorial; cannot be a literal 1
  m = 5

  write(6,'(/,A,I0,A,/,I0,/)') " factorial of ", m, ": ", factorial( m, accum )

  stop
 end program main

