
def factorial ( n, g ):
    if n == 0:
        return 1
    else:
        return n * g( n - 1 , g )


#  main program

print( "\n Please enter an integer:" )

m = int( input() )

print( "\n  %d! = %d\n" % ( m, factorial( m, factorial ) ) )


