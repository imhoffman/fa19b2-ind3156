!!
!!  complement to `tail.c`
!!  $ gfortran -fcray-pointer tail.f90
 module subs
  implicit none
  contains
  subroutine print_stack_address ( n )
          integer, intent(in) :: n
          integer :: dummy

          write(6,'(A,I2,A,Z12)') ' n:', n, ' representative address: ', loc(dummy)

          return
  end subroutine print_stack_address

  subroutine factorial ( n, accum )
          integer :: n, accum
          10 continue
          if ( n .eq. 0 ) then
                  goto 20
          else
                  call print_stack_address( n )
                  accum = accum * n
                  n = n - 1
                  goto 10     !! as with the C, `goto` avoids the need for an aux tail-call
          end if
          20 continue
          return
  end subroutine factorial
 end module subs

 program main
  use subs
  implicit none
  integer :: m, morig, accum

  accum = 1
  m = 5
  morig = m
  call factorial( m, accum )

  write(6,'(A,I0,A,I0)') " factorial of ", morig, ": ", accum

  stop
 end program main

