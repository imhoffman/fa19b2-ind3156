#include<stdio.h>

void print_stack_address ( int n ) {
	register int rsp asm("rsp");
	printf( " n: %d, stack frame address: %x\n", n, rsp );
	return;
}


int factorial ( int n, int accum ) {

	if ( n == 0 ) { return accum; }
	else {
		print_stack_address( n );
		return factorial( n - 1, n * accum );
	}
}


int main ( void ) {

	int m = 5 ;

	printf( "\n factorial of %d: %d\n\n", m, factorial( m, 1 ) );

	return 0;
}
