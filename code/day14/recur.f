!
!  functions
!      
      integer*8 function fact ( m, dummy )
        integer*8 m, dummy
        external dummy           ! dummy is defined elsewhere

        if ( m .eq. 0 ) then
         fact = 1
        else
         fact = m * dummy( m-1, dummy )
        endif

        return
      end function fact
!
!  main program
!
      program recur
        implicit none
        integer*8 n, fact
        external fact           ! fact is defined elsewhere

        write(6,'(/,A)') ' Please enter an integer:'
        read(*,*) n

        write(6,'(/,A,I2,A,I0,/)') ' ',n,'! = ',fact(n,fact)

      end program recur

