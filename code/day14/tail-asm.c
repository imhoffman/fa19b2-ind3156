#include<stdio.h>

void print_stack_address ( int n ) {
	register int rsp asm("rsp");
	printf( " n: %d, stack frame address: %x\n", n, rsp );
	return;
}


int factorial ( int n, int accum ) {
recur:
	if ( n == 0 ) { return accum; }
	else {
		print_stack_address( n );
		accum = accum * n ;
		n     = n - 1 ;
		goto recur ;
	}
}


int main ( void ) {

	int m = 5 ;

	printf( "\n factorial of %d: %d\n\n", m, factorial( m, 1 ) );

	return 0;
}
