!!
!!  complement to `not-tail.c`
!!  $ gfortran -fcray-pointer not-tail.f90
 module subs
  implicit none
  contains
  subroutine print_stack_address ( n )
          integer, intent(in) :: n
          integer :: dummy

          write(6,'(A,I0,A,Z0)') ' n: ', n, ', representative address: 0x', loc(dummy)

          return
  end subroutine print_stack_address

  recursive function factorial ( n ) result ( answer )
          integer :: n, answer
          if ( n .le. 0 ) then
                  answer = 1
          else
                  call print_stack_address( n )
                  answer = n * factorial( n - 1 )
          end if
          return
  end function factorial
 end module subs

 program main
  use subs
  implicit none
  character ( len = 80 ) :: msg
  integer :: m

  m = 5

  write(6,'(/,A,I0,A,/,I0,/)') " factorial of ", m, ": ", factorial( m )

  stop
 end program main

