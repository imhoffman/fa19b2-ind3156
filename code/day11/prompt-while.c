#include<stdio.h>
#include<stdlib.h>

void cli ( void ) {
  int n = 33;
  char arg[n];

  while ( 1 == 1 ) {
    printf( "\nprompt> " );
    fgets( arg, n, stdin );

    if ( arg[0] == 'q' ) {
      printf( " bye\n" );
      break;
    } else {
      printf( " you entered: %s\n", arg );
    }
  }
  return;
}

int main ( void ) {

 cli();

 return 0;
}
