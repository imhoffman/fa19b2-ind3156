 module subs
  implicit none
  contains

  subroutine cli ( )
   character(len=32) :: arg             ! lengthen or shorten 32 as needed

   100 continue
   write(6,*) ''
   write(6,'(A)',advance='no') 'prompt> '
   read(*,*) arg
   if ( index( arg, 'q' ) .eq. 1 ) then    ! anthing that begins with 'q'
     write(6,*) 'bye'
     goto 200
   else
     write(6,*) ' you entered: ', trim(arg)
     goto 100
   endif
   200 continue
  end subroutine cli
 end module subs

!
!  main program
!
 program prompt
  use subs
  implicit none
  
  call cli()

  call exit(0)
  stop
 end program prompt
