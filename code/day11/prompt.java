import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class prompt {
 public static void main(String[] args) throws IOException {
  int e = 1;

  e = cli ( "rpszkq" );

  System.exit( e );
 }


 static int cli ( String allowed ) throws IOException {
  String input;

  repl:
  while ( 1 == 1 ) {
    System.out.format( "%nprompt> " );
    input = (new BufferedReader(new InputStreamReader(System.in))).readLine();

    if ( allowed.indexOf( input.charAt(0) ) == -1 ) {
      continue repl;
    } else if ( input.charAt(0) == 'q' ) {
      System.out.format( " q for quit!%n bye%n" );
      break repl;
    } else {
      System.out.format( " you entered: %s%n", input );
    }
  }

  return 0;
 }

}
