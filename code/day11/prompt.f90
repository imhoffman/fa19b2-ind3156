 module subs
  implicit none
  contains

  recursive subroutine cli ( )
   character(len=32) :: arg             ! lengthen or shorten 32 as needed

   write(6,*) ''
   write(6,'(A)',advance='no') 'prompt> '
   read(*,*) arg
   if ( index( arg, 'q' ) .eq. 1 ) then    ! anthing that begins with 'q'
     write(6,*) 'bye'
     call exit(0)
   else
     write(6,*) ' you entered: ', trim(arg)
     call cli()
   endif
  end subroutine cli
 end module subs

!
!  main program
!
 program prompt
  use subs
  implicit none
  
  call cli()

  call exit(1)      ! the only way out of `cli` should be to quit
  stop
 end program prompt
