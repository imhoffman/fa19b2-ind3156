import sys

##  https://docs.python.org/3/faq/design.html#why-is-there-no-goto
class label(Exception): pass
def cli ( allowed ):
    while ( True ):
        print( " prompt> ", end='', flush=True );
        s = input()
        try:
            if allowed.find( s[0] ) == -1:
                print( "\n commands must begin with: %s\n" % allowed )
                raise label()
            if s[0] == 'q':
                print( " q for quit! bye!" )
                break
            else:
                print( " you typed: ", s )
        except label:
            pass
    return 0

##
##  main program
##
e = cli( "rpszkq" )
sys.exit( e )
