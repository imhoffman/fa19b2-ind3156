#include<stdio.h>
#include<stdlib.h>

void cli ( void ) {
  int n = 33;
  char arg[n];

  printf( "\nprompt> " );
  fgets( arg, n, stdin );

  if ( arg[0] == 'q' ) {
    printf( " bye\n" );
    return;
  } else {
    printf( " you entered: %s\n", arg );
    cli();
  }
}

int main ( void ) {

 cli();

 return 0;
}
