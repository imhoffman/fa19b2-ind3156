 module subs
  implicit none
  contains

  recursive function cli ( ) result ( a )
   integer           :: a
   character(len=32) :: arg             ! lengthen or shorten 32 as needed

   write(6,*) ''
   write(6,'(A)',advance='no') 'prompt> '
   read(*,*) arg
   if ( index( arg, 'q' ) .eq. 1 ) then    ! anthing that begins with 'q'
     write(6,*) 'bye'
     a = 0
     return
   else
     write(6,*) ' you entered: ', trim(arg)
     a = cli()
   endif
  end function cli
 end module subs

!
!  main program
!
 program prompt
  use subs
  implicit none
  integer :: e
 
  e = 1 
  e = cli()

  call exit( e )
 end program prompt
