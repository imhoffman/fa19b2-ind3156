 program array0
  implicit none
  real (kind=8) :: x(12)
  integer       :: i

  do i = 1, 12
   x(i) = real(i,kind=8) * real(i,kind=8)      ! this cast is better than `dble`
  end do

  write(6,*) ''
  write(6,*) x
  write(6,*) ''
  do i = 1, 12
   write(6,'(F14.7)') x(i)
  end do
  write(6,*) ''

  stop
  call exit(0)
 end program array0
