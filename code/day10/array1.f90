 program array1
  implicit none
  integer, parameter :: n = 12
  real (kind=8)      :: x(n)
  integer            :: i

  do i = 1, n
   x(i) = real(i,kind=8) * real(i,kind=8)      ! this cast is better than `dble`
  end do

  write(6,*) ''
  write(6,*) x
  write(6,*) ''
  do i = 1, n
   write(6,'(F14.7)') x(i)
  end do
  write(6,*) ''

  stop
  call exit(0)
 end program array1
