#include<stdio.h>

int main ( void ) {

 double x[12];
 int i;

 for ( i=0; i<12; i++ ) {
  x[i] =(double) i * i;
 }

 printf( "\n" );
 printf( " %f\n", x );
 printf( "\n" );
 for ( i=0; i<12; i++ ) {
  printf( " %f\n", x[i] );
 }

 return 0;
}
