import random, os, sys, math

def f ( x ):
    return math.sqrt( 1.0 - x*x )

def count4me ( n, m ):
    xmin =  0.0
    xmax = +1.0
    ymin =  0.0
    ymax = +1.0
    count4you = 0
    for i in range(n):
        random.seed( os.urandom( m ) )
        x = (xmax-xmin)*random.random() + xmin
        y = (ymax-ymin)*random.random() + ymin
        if ( y < f(x) ):
            count4you = count4you + 1
    return count4you

##
##  main program
##
if ( len(sys.argv) == 2 ):
    ntotal = int( sys.argv[1] )
else:
    ntotal = 8192

ncount = count4me( ntotal, 128 )

print( "\n pi = %+18.16f\n" % float(4.0*ncount/ntotal) )

