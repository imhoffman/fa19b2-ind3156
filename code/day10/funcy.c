#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#include<math.h>
#include<sys/types.h>
#include<unistd.h>

//  boundary function
double f ( double x ) {
 return sqrt( 1.0 - x*x );
}


//  use urandom to populate the seed 
void seeder ( void ) {
 unsigned int nseed;
 FILE *blah = fopen( "/dev/urandom", "rb" );
 fread( &nseed, sizeof( nseed ), 1, blah );
 fclose(blah);
 srand( nseed );
 return;
}


//  cli with a default if ntotal not given by user
//  no checking or creative parsing done here ...
int cli ( int argc, char* argv[] ) {
 if ( argc == 2 ) {
   return atoi( argv[1] );
 } else {
   return 8192;
 }
}


//  main counting loop 
int counter ( int ndarts, double (*derp)( double ),
	       	double xmin, double xmax, double ymin, double ymax ) {
  int ncount=0;
  double x, y;
  for ( int i = 0; i < ndarts; i++ ) {
    x =(double) (xmax-xmin)*(rand() / (RAND_MAX + 1.0))+xmin;
    y =(double) (ymax-ymin)*(rand() / (RAND_MAX + 1.0))+ymin;
    if ( y < derp(x) ) { ncount = ncount + 1; }
  }
  return ncount;
}

//
//  main program
//
int main ( int argc, char* argv[] ) {
 int nTotal, nCounter;

 nTotal = cli( argc, argv );
 seeder();
 nCounter = counter( nTotal, &f,  0.0, +1.0,  0.0, +1.0 ); 
 printf( "\n       pi = %+18.16f\n\n", (double) nCounter/nTotal * 4.0  );
 
 return 0;
}
