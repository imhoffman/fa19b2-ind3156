import random, os, sys, math

def f ( x ):
    return math.sqrt( 1.0 - x*x )

def seeder ( n ):
    random.seed( os.urandom( n ) )
    return

def cli ():
    if ( len(sys.argv) == 2 ):
        return int( sys.argv[1] )
    else:
        return 8192

def counter ( ndarts, func, xmin=0.0, xmax=1.0, ymin=0.0, ymax=1.0 ):
    ncount = 0
    for i in range(ndarts):
        x = (xmax-xmin)*random.random() + xmin
        y = (ymax-ymin)*random.random() + ymin
        if ( y < func(x) ):
            ncount = ncount + 1
    return ncount

##
##  main program
##
ntotal = cli()
seeder(128)
ncount = counter( ntotal, f )
print( "\n pi = %+18.16f\n" % float(4.0*ncount/ntotal) )

