#include<stdio.h>

int main ( void ) {

 int i, n=12;
 double x[n];

 for ( i=0; i<n; i++ ) {
  x[i] =(double) i * i;
 }

 printf( "\n" );
 printf( " %f\n", x );
 printf( "\n" );
 for ( i=0; i<n; i++ ) {
  printf( " %f\n", x[i] );
 }

 return 0;
}
