 module subs
  implicit none
  contains

  subroutine seeder()
   integer              :: i, sdsz
   integer, allocatable :: seed(:)

   call random_seed(size=sdsz)
   allocate( seed( sdsz ) )

   open (unit=10, file='/dev/urandom', access='stream',  &
              form='unformatted', status='old', err=1300)
   do i = 1, sdsz
     read(10,err=1300) seed(i)
   end do
   close(10)

   call random_seed(put=seed)

   1300  continue
   deallocate( seed )
  end subroutine seeder

  pure function f ( x )
   real (kind=8)             :: f
   real (kind=8), intent(in) :: x
   f = sqrt( 1.0D0 - x*x )
  end function f

  function cli ()
   integer :: cli
   character (len=80) :: arg
   if ( command_argument_count() .eq. 1 ) then
    call get_command_argument(1,arg)
    read(arg,*) cli
   else
     cli = 8192
   endif
  end function cli

  function counter ( ndarts, herp, xmin, xmax, ymin, ymax )
    integer                   :: counter
    integer, intent(in)       :: ndarts
    real (kind=8), intent(in) :: xmin, xmax, ymin, ymax
    integer                   :: i
    real (kind=8)             :: x, y
    interface
      pure function herp ( x )
        real(kind=8)              :: herp
        real(kind=8), intent(in)  :: x
      end function herp 
    end interface

    counter = 0
    do i = 1, ndarts
      call random_number(x)
      call random_number(y)
      x = (xmax-xmin)*x + xmin
      y = (ymax-ymin)*y + ymin
      if ( y .lt. herp(x) ) counter = counter + 1
    end do
   end function counter
 end module subs

!
!  main program
!
 program funcy
  use subs
  implicit none
  integer :: ntotal, ncount

  call seeder()

  ntotal = cli()
  ncount = counter ( ntotal, f,  0.0D0, +1.0D0,  0.0D0, +1.0D0 )

  write(6,*) ''
  write(6,'(A12,SPF19.16)') '       pi = ', 4.0D0*dble(ncount)/dble(ntotal)
  write(6,*) ''

  stop
 end program funcy
