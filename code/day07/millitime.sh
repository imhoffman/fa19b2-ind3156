#!/bin/sh
# from https://stackoverflow.com/questions/16959337/usr-bin-time-format-output-elapsed-time-in-milliseconds?rq=1
ti=$(date +%s%N)
$1
tt=$((($(date +%s%N) - $ti)/1000000))
echo "Time taken: $tt milliseconds"
