// x to the yth
// choose values for x and y that do not have simple binary fractions
// x should be greater than zero for this log series
#include<stdio.h>
#include<math.h>

int main(void) {
  double x, y, z, lnx, a, answer;
  x = 1.105;
  y = 1.974;

  a = (x - 1.0)/(x + 1.0);
  lnx = a + a*a*a/3.0 + a*a*a*a*a/5.0
      + a*a*a*a*a*a*a/7.0
      + a*a*a*a*a*a*a*a*a/9.0
      + a*a*a*a*a*a*a*a*a*a*a/11.0
      + a*a*a*a*a*a*a*a*a*a*a*a*a/13.0
      + a*a*a*a*a*a*a*a*a*a*a*a*a*a*a/15.0
      + a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a/17.0
      + a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a/19.0
      + a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a/21.0
      + a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a*a/23.0
   ;

  z = y*2.0*lnx;
  answer = 1.0 + z + z*z/2.0 + z*z*z/6.0
       + z*z*z*z/24.0
       + z*z*z*z*z/1.2E2
       + z*z*z*z*z*z/7.2E2
       + z*z*z*z*z*z*z/5.04E3
       + z*z*z*z*z*z*z*z/4.032E4
       + z*z*z*z*z*z*z*z*z/3.6288E5
       + z*z*z*z*z*z*z*z*z*z/3.6288E6
       + z*z*z*z*z*z*z*z*z*z*z/3.6288E6/1.1E1
       + z*z*z*z*z*z*z*z*z*z*z*z/3.6288E6/1.1E1/1.2E1
       + z*z*z*z*z*z*z*z*z*z*z*z*z/3.6288E6/1.1E1/1.2E1/1.3E1
       + z*z*z*z*z*z*z*z*z*z*z*z*z*z/3.6288E6/1.1E1/1.2E1/1.3E1/1.4E1
    ;

  printf("\n   manual %23.20f\n", answer);
  printf(" built-in %23.20f\n", pow(x,y));
  printf("                          ^\n");
  printf("   expected 64-bit accuracy\n\n");

  return 0;
}
