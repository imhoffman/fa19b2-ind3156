! x to the yth power for float y
! choose values for x and y that do not have simple binary fractions
! x should be between zero and two for this log series
 program exlnb
  implicit none
  real (kind=8) :: x, y, z, lnx, a, answer
  x = 1.105D0
  y = 1.974D0

      a = x - 1.0D0
      lnx = a - a*a/2.0D0 + a*a*a/3.0D0 - a*a*a*a/4.0D0 &
           + a*a*a*a*a/5.0D0                            &
           - a*a*a*a*a*a/6.0D0                          &
           + a*a*a*a*a*a*a/7.0D0                        & 
           - a*a*a*a*a*a*a*a/8.0D0                      &
           + a*a*a*a*a*a*a*a*a/9.0D0                    &
           - a*a*a*a*a*a*a*a*a*a/1.0D1                  &
           + a*a*a*a*a*a*a*a*a*a*a/1.1D1                &
           - a*a*a*a*a*a*a*a*a*a*a*a/1.2D1              &
           + a*a*a*a*a*a*a*a*a*a*a*a*a/1.3D1            &
           - a*a*a*a*a*a*a*a*a*a*a*a*a*a/1.4D1
!           + a*a*a*a*a*a*a*a*a*a*a*a*a*a*a/1.5D1

      z = y*lnx
      answer = 1.0D0 + z + z*z/2.0D0 + z*z*z/6.0D0      &
            + z*z*z*z/2.4D1                             &
            + z*z*z*z*z/1.2D2                           &
            + z*z*z*z*z*z/7.2D2                         &
            + z*z*z*z*z*z*z/5.04D3                      &
            + z*z*z*z*z*z*z*z/4.032D4                   & 
            + z*z*z*z*z*z*z*z*z/3.6288D5                &
            + z*z*z*z*z*z*z*z*z*z/3.6288D6              &
            + z*z*z*z*z*z*z*z*z*z*z/3.6288D6/1.1D1      &
            + z*z*z*z*z*z*z*z*z*z*z*z/3.6288D6/1.1D1/1.2D1 &
            + z*z*z*z*z*z*z*z*z*z*z*z*z/3.6288D6/1.1D1/1.2D1/1.3D1
!            + z*z*z*z*z*z*z*z*z*z*z*z*z*z/3.6288D6/1.1D1/1.2D1/1.3D1/1.4D1

  write(6,*) ''
  write(6,'(A10,F23.20)') '   manual ', answer
  write(6,'(A10,F23.20)') ' built-in ', x**y
  write(6,*) '                          ^'
  write(6,*) '   expected 64-bit accuracy'
  write(6,*) ''

  stop
 end program exlnb

