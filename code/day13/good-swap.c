#include<stdio.h>

//  the subroutine has been changed to accept addresses rather than values
void swap( int *one, int *two ) {
 int temp;

 temp = *one;
 *one = *two;
 *two = temp;

 return;
}


int main( void ) {
 int  i,  j;
 int *q, *r;

 i = 5;
 j = 8;

 printf("     i,j before the swap: %d, %d\n", i, j);
 //  this call now is given addresses
 swap( &i, &j );
 printf("      i,j after the swap: %d, %d\n", i, j);
 q = &i;
 r = &j;
 //  the following is no different than passing &i, &j
 swap( q, r );
 printf("  i,j after another swap: %d, %d\n", i, j);

 return 0;
}
