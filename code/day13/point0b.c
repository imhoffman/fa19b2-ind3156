#include<stdio.h>

int main(void) {
 int j, i;
 int *q;

 j = 19;
 q = &j;    //  like `q = loc(j)`
 i = *q;    //  like `pointer (q,i)

 printf( "\n The memory address of the integer is %p\n", q );
 printf( "          The value of the integer is %d\n\n", i );
 //  the next output likely differs between gcc, icc, and clang
 printf( " The memory address %d bytes away has integer %d\n\n", 2*(int)sizeof(i), *(q+2) );

 return 0;
}
