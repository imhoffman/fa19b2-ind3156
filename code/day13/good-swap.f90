 subroutine swap( one, two )
  integer, intent(inout) :: one, two
  integer                :: temp

  temp = one
  one = two
  two = temp

  return
 end subroutine swap

 program point
  implicit none

  integer, target  :: i, j
  integer, pointer :: q, r

  i = 5
  j = 8

  write(6,*) '    i,j before the swap:', i, j
  call swap( i, j )   ! no ampersands or asterisks ... everything is by reference
  write(6,*) '     i,j after the swap:', i, j
  q => i
  r => j
  call swap( q, r )   ! no ampersands or asterisks ... everything is by reference
  write(6,*) ' i,j after another swap:', i, j

 end program point

