!  gfortran point0.f -fcray-pointer
      program point0
      implicit none

      integer*4 j, i
      pointer (q, i)       ! like  `q = &i`  or  `i = *q`

      j = 19
      q = loc(j)           ! like  `q = &j`
      write(6,'(/,A,Z12)') ' The memory address of the integer is 0x',q
      write(6,'(A,I2,/)') '          The value of the integer is ', i

      stop
      end program point0
