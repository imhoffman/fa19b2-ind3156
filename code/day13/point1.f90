 program point1
  implicit none

  integer, target  :: i, j
  integer, pointer :: q

  j =  19
  q => j      ! this is like `q=loc(j)` or C `q = &j`
  i =  q      ! this is like C `i = *q`

  ! whenever q is an lvalue, it acts like an address
  ! whenever q is an rvalue---an evaluatable expression---is acts like the derferenced value

  ! Fortran 90 does not act like the Cray addressing from the point0.f example
  write(6,'(/,A,Z12)') ' The memory address of the integer is 0x',q
  write(6,'(A,I2)')    ' The memory address of the integer is ',q
  write(6,'(A,I2,/)')  '          The value of the integer is ', i

  stop
 end program point1
