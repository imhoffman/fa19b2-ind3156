  program cli
  implicit none

  integer (kind=2), parameter   :: n = 16
  character (len=n)   :: arg
  real (kind=8)       :: arg1, arg2, arg3
  integer             :: fail=0

  if ( command_argument_count() .ne. 3 ) then
    write(6,*) " You're doing it wrong."
    call exit(1)
  end if
  call get_command_argument(1,arg,status=fail)
  if ( fail .eq. 0 ) read(arg,*) arg1
  call get_command_argument(2,arg,status=fail)
  if ( fail .eq. 0 ) read(arg,*) arg2
  call get_command_argument(3,arg,status=fail)
  if ( fail .eq. 0 ) read(arg,*) arg3
  write(6,'(A,F5.2,A,F5.2,A,F5.2)') ' your reals are ', arg1,', ',arg2,', and ',arg3

  stop
  end program cli
