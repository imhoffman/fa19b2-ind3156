  program cli
  implicit none

  integer (kind=2), parameter   :: n = 16
  character (len=n)   :: arg
  integer             :: arg1, fail=0

  if ( command_argument_count() .ne. 1 ) then
    write(6,*) " You're doing it wrong."
    call exit(0)
  end if

  call get_command_argument(1,arg,status=fail)
  if ( fail .eq. 0 ) then
   read(arg,*) arg1
   call exit( arg1 )
  else
   call exit(1)
  endif

  stop
  end program cli
