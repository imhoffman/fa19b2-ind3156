#include<stdio.h>
#include<stdlib.h>
 
int main(int argc, char *argv[]) {
 double arg1, arg2, arg3;
 char *ptr;

 if ( argc != 4 ) {
  printf("\n You're doing it wrong.\n\n");
  return 1;
 } else {
  arg1 = strtod( argv[1], &ptr);
  arg2 = strtod( argv[2], &ptr);
  arg3 = strtod( argv[3], &ptr);

  printf("\n your doubles are %+f, %+f, and %+f.\n\n", arg1, arg2, arg3);
  
  return 0;
 }
}
