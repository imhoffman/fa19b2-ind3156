import sys

if ( len(sys.argv) != 4 ):
        print("\n You're doing it wrong.\n")
        sys.exit()

# this parsing won't catch strings, etc.
a = float( sys.argv[1] )
b = float( sys.argv[2] )
c = float( sys.argv[3] )

print("\n your doubles are %f, %f, and %f\n" % (a, b, c) )
