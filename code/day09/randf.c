#include<stdlib.h>
#include<stdio.h>
#include<time.h>
#include<math.h>
#include<sys/types.h>
#include<unistd.h>

//  boundary function
double f ( double x ) {
 return sqrt( 1.0 - x*x );
}

//
//  main program
//
int main ( int argc, char* argv[] ) {
 int i, nTotal, nCounter=0;
 unsigned int nseed;
 double x, y;
 double xmin, xmax, ymin, ymax;
 pid_t getpid(void);

 //  scaling for distribution ... not actually necessary for (0,1) 
 xmin =  0.0;
 xmax = +1.0;
 ymin =  0.0;
 ymax = +1.0;

 //  use urandom to populate the seed 
 FILE *blah = fopen( "/dev/urandom", "rb" );
 fread( &nseed, sizeof( nseed ), 1, blah );
 fclose(blah);
 //srand(time(NULL) + getpid());
 srand( nseed );

 //  cli with a default if ntotal not given by user
 //  no checking or creative parsing done here ...
 if ( argc == 2 ) {
   nTotal = atoi( argv[1] );
 } else {
   nTotal = 8192;
 }

 //  main counting loop 
 for ( i=0; i < nTotal; i++ ) {

  x =(double) (xmax-xmin)*(rand() / (RAND_MAX + 1.0))+xmin;
  y =(double) (ymax-ymin)*(rand() / (RAND_MAX + 1.0))+ymin;

  if ( y < f(x) ) { nCounter = nCounter + 1; }

 }
 //  end of main counting loop

 //  report four times the result since only first quadrant is used
 printf( "\n       pi = %+18.16f\n\n", (double) nCounter/nTotal * 4.0  );
 
 return 0;
}
