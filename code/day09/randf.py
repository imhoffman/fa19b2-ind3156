import random, os, sys, math

def f ( x ):
    return math.sqrt( 1.0 - x*x )

random.seed( os.urandom( 128 ) )

xmin =  0.0
xmax = +1.0
ymin =  0.0
ymax = +1.0

if ( len(sys.argv) == 2 ):
    ntotal = int( sys.argv[1] )
else:
    ntotal = 8192

ncount = 0
for i in range(ntotal):
    x = (xmax-xmin)*random.random() + xmin
    y = (ymax-ymin)*random.random() + ymin
    if ( y < f(x) ):
        ncount = ncount + 1

print( "\n       pi = %+18.16f\n" % float(4.0*ncount/ntotal) )

