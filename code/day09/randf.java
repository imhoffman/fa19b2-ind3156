import java.util.Random;

public class randf {
 public static void main(String[] args) {

  int i, nTotal, nCounted=0;
  double x, y, xmin, xmax, ymin, ymax;
  Random r;

  xmin = +0.0;
  xmax = +1.0;
  ymin = +0.0;
  ymax = +1.0;

  r = new Random( System.currentTimeMillis() );

  //nTotal = 8192;
  nTotal = Integer.parseInt( args[0] );

  for ( i=0; i < nTotal; i++ ) {

   x = (xmax-xmin)*r.nextDouble()+xmin;
   y = (ymax-ymin)*r.nextDouble()+ymin;

   if ( y < f(x) ) { nCounted = nCounted + 1; }

  }

  System.out.format("%n       pi = %+19.16f%n%n", 4.0*(double)nCounted/nTotal );

  System.exit( 0 );
 }


 static double f ( double xf ) {
	 return Math.sqrt( 1.0 - xf*xf );
 }


}
