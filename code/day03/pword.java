//
// $ echo -n "P@S5w0rD" | md5sum -
// 430cc5e8ac97701e5e8c3bc85b9f9174  -
//
//
//  change ifne (9a) to ifeq (99) in the if/else
//  $ javap -c pword.class
//  $ vim -b pword.class
//  https://en.wikipedia.org/wiki/Java_bytecode_instruction_listings
//
import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class pword {
 public static void main(String[] args) throws NoSuchAlgorithmException {
    String storedHash = "430cc5e8ac97701e5e8c3bc85b9f9174";
    
    if ( args.length != 1 ) {
	System.out.println(" Please enter the password.");
        System.exit(1);
    }
         
    MessageDigest md = MessageDigest.getInstance("MD5");
    byte[] pwordHashBytes = md.digest( args[0].getBytes() );
    String pwordHash = DatatypeConverter.printHexBinary(pwordHashBytes).toLowerCase();

    System.out.format( "%n     Your password's hash: %s%n", pwordHash );
    System.out.format( " The stored password hash: %s%n", storedHash );

    System.out.println();
    if ( pwordHash.compareTo(storedHash) == 0 ) {
      System.out.format( " You know the password! The bank account has been emptied.%n%n" );
      System.exit(0);
    } else {
      System.out.format( " That password is incorrect. You are in trouble.%n%n" );
      System.exit(2);
    }
 }
}

