#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<openssl/md5.h>
//
// $ echo -n "P@S5w0rD" | md5sum -
// 430cc5e8ac97701e5e8c3bc85b9f9174  -
//
//  cc pword.c -l crypto   ; try gcc, icc, and clang
//  $ objdump -d a.out | less
//  $ vim -b a.out
//
//  x86: https://c9x.me/x86/html/file_module_x86_id_146.html
//  change jne (75 or 0f 85) to je (74 or 0f 84) after the strcmp
//  ARM: https://static.docs.arm.com/ddi0487/ea/DDI0487E_a_armv8_arm.pdf
//  Table C1-1 for Conditions ... switch among EQ, NE, and AL
//

int main ( int argc, char *argv[] ) {
    const unsigned char *pword = NULL;
    char *storedHash = "430cc5e8ac97701e5e8c3bc85b9f9174";
    char pwordHash[ 2*MD5_DIGEST_LENGTH + 1 ];    // length of 33
    
    if ( argc != 2 ) {
	printf("\n Please enter the password.\n\n");
        return 1;
    }
    pword = (const unsigned char *) argv[1];  // argv can't be the const that MD5 wants

    unsigned char md[MD5_DIGEST_LENGTH];
    MD5( pword, strlen( (const char *)pword )*sizeof( char ), md );

    //  sprintf adds a trailing \0 after each iteration; overwrite all but the last
    for ( int i=0; i<MD5_DIGEST_LENGTH; i++ ) { sprintf( &pwordHash[2*i], "%02hhx", md[i] ); }

    printf( "\n     Your password's hash: %s\n", pwordHash );
    printf( " The stored password hash: %s\n\n", storedHash );

    if ( strcmp( pwordHash, storedHash ) == 0 ) {
      printf( " You know the password! The bank account has been emptied.\n\n" );
      return 0;
    } else {
      printf( " That password is incorrect. You are in trouble.\n\n" );
      return 2;
    }
}

