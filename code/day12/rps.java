import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.IOException;
import java.util.Random;

public class rps {
 public static void main(String[] args) throws IOException {
  int e = 1;
  Random r = new Random( System.currentTimeMillis() );
  int[] score = { 0, 0, 0 };
  String history = "pzpkrrs";     // fake history --- implement later

  System.out.format( "\n\n  Ro-sham-bo !!\n\n" );
  e = cli ( "rpszkq", r, score, history );

  System.exit( e );
 }


 //  gameplay prompt loop interface
 static int cli ( String allowed, Random rand, int[] score, String history ) throws IOException {
  String input;
  char cpu;

  repl:
  while ( 1 == 1 ) {
    scoreboard( score, false );
    prompt();
    input = (new BufferedReader(new InputStreamReader(System.in))).readLine();

    //  check for blank or useless inputs
    if ( input.isEmpty() ) { continue repl; }
    if ( allowed.indexOf( input.charAt(0) ) == -1 ) { continue repl; }

    //  quit or play
    if ( input.charAt(0) == 'q' ) {
      System.out.format( "\n farewell!\n" );
      scoreboard( score, true );
      break repl;
    } else {
      cpu = chooser( rand, history );
      score = scorer( input.charAt(0), cpu, score );
    }
  }
  return 0;
 }

 //  strategy routine --- evenly random
 static double[] strategy ( String history ) {
   double[] bounds = { 0.2, 0.4, 0.6, 0.8 };
   return bounds;
 }

 //  implementation of the `strategy` routine
 static char chooser ( Random r, String history ) {
   double[] bounds = strategy( history );
   double x = r.nextDouble();
   char out;

   if        (       0.0 <= x  &&  x < bounds[0] ) { out = 'r';
   } else if ( bounds[0] <= x  &&  x < bounds[1] ) { out = 'p';
   } else if ( bounds[1] <= x  &&  x < bounds[2] ) { out = 's';
   } else if ( bounds[2] <= x  &&  x < bounds[3] ) { out = 'z';
   } else { out = 'k'; }

   return out;
 }

 //  scoring routine
 //   returns a new array b/c of pass-by-value
 static int[] scorer ( char opp, char cpu, int[] score ) {
    System.out.format( "  you throw %s;   cpu throws %s\n", words(opp), words(cpu) );
    if ( opp == cpu ) {
	    System.out.format( "  this round is a draw!\n" );
	    score[1] = score[1] + 1;
    } else if ( ( opp == 'r' && ( cpu == 's' || cpu == 'z' ) ) ||
                ( opp == 'p' && ( cpu == 'r' || cpu == 'k' ) ) ||
                ( opp == 's' && ( cpu == 'p' || cpu == 'z' ) ) ||
                ( opp == 'z' && ( cpu == 'p' || cpu == 'k' ) ) ||
                ( opp == 'k' && ( cpu == 'r' || cpu == 's' ) ) ) {
	    System.out.format( "  you win this round!\n" );
	    score[0] = score[0] + 1;
    } else {
	    System.out.format( "  you lose this round!\n" );
	    score[2] = score[2] + 1;
    }
    return score;
 }

 //  look-up table for meaning of characters
 static String words ( char a ) {
    if        ( a == 'r' ) { return "rock";
    } else if ( a == 'p' ) { return "paper";
    } else if ( a == 's' ) { return "scissors";
    } else if ( a == 'z' ) { return "lizard";
    } else if ( a == 'k' ) { return "Spock";
    } else {
	    System.out.format( " something wrong with character\n" );
	    System.exit( 2 );
	    return "problem";   // won't get here --- simply for completeness
    }
 }

 //  display score 
 static void scoreboard ( int[] score, boolean quit ) {
    if ( quit ) {
        System.out.format( "\n   final score ....... you: %d/%d/%d :cpu\n\n",
		 score[0], score[1], score[2] );
    } else {
        System.out.format( "\n current score ....... you: %3d/%3d/%3d :cpu\n",
		 score[0], score[1], score[2] );
    }
    return;
 }

 //  contents of repetitive gameplay display
 static void prompt ( ) {
    System.out.format( "  (r)ock, (p)aper, (s)cissors, li(z)ard, Spoc(k) ... or (q)uit\n" );
    System.out.format( " >Ro-sham-bo> " );
    return;
 }

}
