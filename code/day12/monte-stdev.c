#include<stdlib.h>
#include<stdio.h>
#include<math.h>

//  boundary function
double f ( double x ) {
 return sqrt( 1.0 - x*x );
}


//  use urandom to populate the seed 
void seeder ( void ) {
 unsigned int nseed;
 FILE *blah = fopen( "/dev/urandom", "rb" );
 fread( &nseed, sizeof( nseed ), 1, blah );
 fclose(blah);
 srand( nseed );
 return;
}

//  cli with a simple error message and return --- don't use unsigned ints
//  no creative parsing done here ...
void cli ( int c, char* v[], int* m, int* n ) {
 if ( c == 3 ) {
   *m = atoi( v[1] );
   *n = atoi( v[2] );
 } else {
   *m = -1;
   *n = -1;
   printf( "\n usage: a.out nDarts nTrials\n\n" );
 }
 return;
}


//  main counting loop 
double* counter ( int ndarts, int ntrials, double (*derp)( double ),
	       	double xmin, double xmax, double ymin, double ymax ) {
  static double output[2];
  double area[ntrials];
  double x, y;
  int ncount;
  double avg, stdev;

  for ( int j=0; j < ntrials; j++ ) {
    ncount = 0;
    for ( int i = 0; i < ndarts; i++ ) {
      x =(double) (xmax-xmin)*(rand() / (RAND_MAX + 1.0))+xmin;
      y =(double) (ymax-ymin)*(rand() / (RAND_MAX + 1.0))+ymin;
      if ( y < derp(x) ) { ncount = ncount + 1; }
    }
    area[j] =(double) ncount/ndarts * 4.0;
  }

  avg = 0.0;
  for ( int j=0; j < ntrials; j++ ) {
    avg = avg + area[j];
  }
  avg = avg / (double)ntrials;

  stdev = 0.0;
  for ( int j=0; j < ntrials; j++ ) {
   stdev = stdev + (area[j] - avg)*(area[j] - avg);
  }
  stdev =(double) sqrt( stdev / ntrials );

  output[1] = stdev;
  output[0] = avg;

  return output;
}

//
//  main program
//
int main ( int argc, char* argv[] ) {
 int ndarts, ntrials, ncounter;
 double* stats;

 cli( argc, argv, &ndarts, &ntrials );
 if ( ndarts == -1 ) { return 1; }
 seeder();
 stats = counter( ndarts, ntrials, &f,  0.0, +1.0,  0.0, +1.0 ); 

 printf( "\n       pi = %+18.16f\n",  *( stats + 0 )  );
 printf( "\n        +/- %19.16f\n\n", *( stats + 1 )  );
 
 return 0;
}
