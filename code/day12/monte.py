import random, os, sys, math

def f ( x ):
    return math.sqrt( 1.0 - x*x )

def seeder ( n ):
    random.seed( os.urandom( n ) )
    return

def cli ():
    if ( len(sys.argv) == 3 ):
        return int( sys.argv[1] ), int( sys.argv[2] )
    else:
        print( "\n usage: python monte.py ndarts ntrials\n\n" )
        return -1, -1

def counter ( ndarts, ntrials, func, xmin=0.0, xmax=1.0, ymin=0.0, ymax=1.0 ):
    area = []
    for j in range(ntrials):
        ncount = 0
        for i in range(ndarts):
            x = (xmax-xmin)*random.random() + xmin
            y = (ymax-ymin)*random.random() + ymin
            if ( y < func(x) ):
                ncount = ncount + 1
        area.append( float( 4.0*ncount/ndarts ) )

    avg = sum( [ x for x in area ] )
    avg = avg / float(ntrials)

    return avg

##
##  main program
##
ndarts, ntrials = cli()
if ndarts == -1: sys.exit(1)
seeder(128)
avg = counter( ndarts, ntrials, f )
print( "\n pi = %+18.16f\n" % avg )

