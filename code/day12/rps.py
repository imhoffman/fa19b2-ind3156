import random, os, sys

def seeder ( n ):
    random.seed( os.urandom( n ) )
    return

def scoreboard ( score, quit ):
    if quit:
        print( "\n   final score ....... you: %d/%d/%d :cpu\n"
                % (score[0], score[1], score[2]) )
    else:
        print( "\n current score ....... you: %3d/%3d/%3d :cpu"
                % (score[0], score[1], score[2]) )

def prompt ( ):
 print( "  (r)ock, (p)aper, (s)cissors, li(z)ard, Spoc(k) ... or (q)uit" )
 print( " >Ro-sham-bo> ", end='', flush=True )
 return

words = { 'r': "rock",
          'p': "paper",
          's': "scissors",
          'z': "lizard",
          'k': "Spock" }

##  https://www.python.org/dev/peps/pep-0008/
def scorer ( opp, cpu, score ):
    print( "  you throw %s;   cpu throws %s" % ( words[opp], words[cpu] ) )
    if opp == cpu:
        print( "  this round is a draw!" )
        score[1] = score[1] + 1
    elif ( opp == 'r' and ( cpu == 'z' or  cpu == 's' ) ) or \
         ( opp == 'p' and ( cpu == 'k' or  cpu == 'r' ) ) or \
         ( opp == 's' and ( cpu == 'z' or  cpu == 'p' ) ) or \
         ( opp == 'z' and ( cpu == 'k' or  cpu == 'p' ) ) or \
         ( opp == 'k' and ( cpu == 'r' or  cpu == 's' ) ):
        print( "  you win this round!" )
        score[0] = score[0] + 1
    else: 
        print( "  you lose this round!" )
        score[2] = score[2] + 1
    return score

def strategy ( history ):
    ##  for later: somewhere address history as a queue
    ##  https://docs.python.org/3.7/tutorial/datastructures.html#using-lists-as-queues
    return [ float(i)/5.0 for i in range(1,5) ]    # simple random

def chooser ( history ):
    bounds = strategy( history )
    x = random.random()
    if         0.0 <= x  and  x < bounds[0]: return 'r'
    elif bounds[0] <= x  and  x < bounds[1]: return 'p'
    elif bounds[1] <= x  and  x < bounds[2]: return 's'
    elif bounds[2] <= x  and  x < bounds[3]: return 'z'
    else: return 'k'

##  https://docs.python.org/3/faq/design.html#why-is-there-no-goto
class label(Exception): pass
def cli ( score, history ):
    while ( True ):
        scoreboard( score, False )
        prompt()
        s = input()
        try:
            if not s: raise label()
            if "rpszkq".find( s[0] ) == -1: raise label()
            if s[0] == 'q':
                print( "\n  farewell!" )
                scoreboard( score, True )
                break
            else:
                cpu = chooser( history )
                score = scorer( s[0], cpu, score )
        except label:
            pass
    return 0

##
##  main program
##
seeder(128)
e = cli( [0, 0, 0], "rkrkzps" )       # fake history ... implement later
sys.exit( e )
