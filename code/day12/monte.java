import java.util.Random;

public class monte {
 public static void main( String[] args ) {

  int[] user = new int[2];
  int ndarts, ntrials;
  double answer;

  user = cli ( args );
  if ( user[0] == -1 ) { System.exit( 1 ); }
  ndarts = user[0];
  ntrials = user[1];

  Random r = new Random( System.currentTimeMillis() );
  answer = counter( ndarts, ntrials, r,  0.0, +1.0,  0.0, +1.0 );
  System.out.format( "%n       pi = %+18.16f%n%n", answer );

  System.exit( 0 );
 }


 //  check command line for worthwhile inputs
 static int[] cli ( String[] a ) {
  int[] n = new int[2];
  if ( a.length == 2 ) {
    n[0] = Integer.parseInt( a[0] );
    n[1] = Integer.parseInt( a[1] );
  } else {
    System.out.format( "%n usage: java monte ndarts ntrials%n%n" );
    n[0] = -1; n[1] = -1;
  }
  return n;
 }


 //  the function being quadratured
 static double f ( double x ) {
	 return Math.sqrt( 1.0 - x*x );
 }


 //  the main counting loop
 static double counter ( int ndarts, int ntrials, Random rand,
	       	double xmin, double xmax, double ymin, double ymax ) {
  double[] area = new double[ntrials];
  double x, y;
  int ncount;
  double avg;

  for ( int j=0; j < ntrials; j++ ) {
    ncount = 0;
    for ( int i = 0; i < ndarts; i++ ) {
      x = (xmax-xmin)*rand.nextDouble() + xmin;
      y = (ymax-ymin)*rand.nextDouble() + ymin;
      if ( y < f(x) ) { ncount = ncount + 1; }
    }
    area[j] =(double) ncount/ndarts * 4.0;
  }

  avg = 0.0;
  for ( int j=0; j < ntrials; j++ ) {
    avg = avg + area[j];
  }
  avg = avg / (double)ntrials;

  return avg;
 }

}
