 module subs
  implicit none
  contains

  subroutine seeder()
   integer              :: i, sdsz
   integer, allocatable :: seed(:)

   call random_seed(size=sdsz)
   allocate( seed( sdsz ) )

   open (unit=10, file='/dev/urandom', access='stream',  &
              form='unformatted', status='old', err=1300)
   do i = 1, sdsz
     read(10,err=1300) seed(i)
   end do
   close(10)

   call random_seed(put=seed)

   1300  continue
   deallocate( seed )
  end subroutine seeder

  pure function f ( x )
   real (kind=8)             :: f
   real (kind=8), intent(in) :: x
   f = sqrt( 1.0D0 - x*x )
  end function f

  subroutine cli ( arg1, arg2 )
   integer, intent(inout) :: arg1, arg2
   character (len=80)     :: arg
   if ( command_argument_count() .eq. 2 ) then
     call get_command_argument(1,arg)
     read(arg,*) arg1
     call get_command_argument(2,arg)
     read(arg,*) arg2
   else
     write(6,'(/,A,/)') ' usage: a.out ndarts ntrials'
     arg1 = -1
   endif
  end subroutine cli

  function counter ( ndart, ntrial, herp, xmin, xmax, ymin, ymax ) result ( average )
    real (kind=8)             :: average
    integer, intent(in)       :: ndart, ntrial
    real (kind=8), intent(in) :: xmin, xmax, ymin, ymax
    integer                   :: i, j, ncount
    real (kind=8)             :: x, y, area(ntrial)
    interface
      pure function herp ( x )
        real(kind=8)              :: herp
        real(kind=8), intent(in)  :: x
      end function herp 
    end interface

    do j = 1, ntrial
      ncount = 0
      do i = 1, ndart
        call random_number(x)
        call random_number(y)
        x = (xmax-xmin)*x + xmin
        y = (ymax-ymin)*y + ymin
        if ( y .lt. herp(x) ) ncount = ncount + 1
      end do
      area(j) = 4.0D0 * real( ncount, kind=8 ) / real( ndart, kind=8 )
    end do

    average = 0.0D0
    do j = 1, ntrial
      average = average + area(j)
    end do
    average = average / real( ntrial, kind=8 )
   end function counter
 end module subs

!
!  main program
!
 program monte
  use subs
  implicit none
  integer       :: ndarts, ntrials
  real (kind=8) :: avg

  call seeder()

  call cli( ndarts, ntrials )
  if ( ndarts .eq. -1 ) call exit( 1 )

  avg = counter ( ndarts, ntrials, f,  0.0D0, +1.0D0,  0.0D0, +1.0D0 )

  write(6,'(/,A12,SPF19.16,/)') '       pi = ', avg

  stop
 end program monte
