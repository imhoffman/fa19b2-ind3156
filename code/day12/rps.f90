 module subs
  implicit none
  character(*), parameter :: poss = 'rpszkq'
  character(*), parameter :: play = 'rpszk'
  contains

  subroutine seeder()
   integer              :: i, sdsz
   integer, allocatable :: seed(:)
   call random_seed(size=sdsz)
   allocate( seed( sdsz ) )
   open (unit=10, file='/dev/urandom', access='stream',  &
              form='unformatted', status='old', err=1300)
   do i = 1, sdsz
     read(10,err=1300) seed(i)
   end do
   close(10)
   call random_seed(put=seed)
   1300  continue
   deallocate( seed )
  end subroutine seeder


  function worder ( c ) result ( word )
    character, intent(in)     :: c
    character(:), allocatable :: word
    if ( c.ne.'r' .and. c.ne.'p' .and. c.ne.'s' &
           .and. c.ne.'z' .and. c.ne.'k' ) then
            call exit(3)     ! this shouldn't happen if called properly
    elseif ( c.eq.'r' ) then
      word = 'rock'
    elseif ( c.eq.'p' ) then
      word = 'paper'
    elseif ( c.eq.'s' ) then
      word = 'scissors'
    elseif ( c.eq.'z' ) then
      word = 'lizard'
    elseif ( c.eq.'k' ) then
      word = 'Spock'
    else
      write(6,*) ' somebody didn''t migrate to lizard, Spock'
      call exit(4)
    end if
  end function worder

  subroutine instructions ()
    write(6,*) ' (r)ock, (p)aper, (s)cissors, li(z)ard, Spoc(k) ... or (q)uit'
  end subroutine instructions

  subroutine scoreboard ( score, bye )
    integer, intent(in) :: score(3)
    logical, intent(in) :: bye
    character(len=80)   :: str
    if ( bye ) then
      write( str,'(A,I0,A,I0,A,I0,A)') &
        '   final score ....... you: ', &
          score(1), '/', score(2), '/', score(3), ': cpu'
      write( 6,* ) ''
      write( 6,* ) ' farewell!'
      write( 6,* ) ''
      write( 6, '(A)' ) trim(str)
      write( 6,* ) ''
      open( unit=11, file='score.txt' )
      write(11, '(A)' ) trim(str)
      close(11)
    else
      write(6,'(A,I3,A,I3,A,I3,A)') &
        ' current score ....... you: ', &
          score(1), '/', score(2), '/', score(3), ': cpu'
    endif
  end subroutine scoreboard

  subroutine scorer ( you, cpu, score )
    character, intent(in)  :: you, cpu
    integer, intent(inout) :: score(:)    ! assumed to be size 3

    write(6,*) ' you throw: ', worder(you), ';  cpu throws: ', worder(cpu)
    if ( you .eq. cpu ) then
      write(6,*) ' this round is a draw!'
      score(2) = score(2) + 1
      return
    elseif ( ( you.eq.'r' .and. ( cpu.eq.'s' .or. cpu.eq.'z' ) ) .or. &
             ( you.eq.'s' .and. ( cpu.eq.'p' .or. cpu.eq.'z' ) ) .or. &
             ( you.eq.'z' .and. ( cpu.eq.'p' .or. cpu.eq.'k' ) ) .or. &
             ( you.eq.'k' .and. ( cpu.eq.'r' .or. cpu.eq.'s' ) ) .or. &
             ( you.eq.'p' .and. ( cpu.eq.'r' .or. cpu.eq.'k' ) ) ) then
            write(6,*) ' you win this round!'
            score(1) = score(1) + 1
            return
    else
      write(6,*) ' you lose this round!'
      score(3) = score(3) + 1
      return
    endif
    return
  end subroutine scorer

  recursive subroutine cli ( past, score )
   real (kind=8), intent(inout) :: past(:)
   integer, intent(inout)       :: score(:)
   character                    :: s, cpu

   cpu = chooser( past )    ! but don't update `past` until after a legit prompt entry

   write(6,*) ''
   call scoreboard( score, .false. )
   call instructions()
   write(6,'(A)',advance='no') ' >Ro-Sham-Bo> '
   read(*,*) s
   if ( index( poss, s ) .eq. 0 ) then
     write(6,*) ' invalid entry.'
     call cli( past, score )
   elseif ( index( play, s ) .ne. 0 ) then
     call scorer( s, cpu, score )
     call cli( past, score )
   elseif ( s.eq.'q' ) then
     call scoreboard( score, .true. )
     call exit(0)
   else
     call exit(1)      ! shouldn't get here
   endif
  end subroutine cli


  subroutine strategy ( past, boundaries )
    real (kind=8), intent(in)    :: past(:)
    real (kind=8), intent(inout) :: boundaries(:)
    integer                      :: i
    
    do i = 1, size( boundaries )
     boundaries(i) = real(i,kind=8)/( 1.0D0 + real(size(boundaries),kind=8) )
    end do

    return
  end subroutine strategy


  function chooser ( past )
    character                  :: chooser
    real (kind=8), intent(in)  :: past(:)
    real (kind=8)              :: x
    real (kind=8), allocatable :: boundaries(:)

    allocate( boundaries(4) )
    call strategy( past, boundaries )

    call random_number(x)
    if             ( 0.0D0.le.x .and. x.lt.boundaries(1) ) then
            chooser = 'r'
    elseif ( boundaries(1).le.x .and. x.lt.boundaries(2) ) then
            chooser = 'p'
    elseif ( boundaries(2).le.x .and. x.lt.boundaries(3) ) then
            chooser = 'z'
    elseif ( boundaries(3).le.x .and. x.lt.boundaries(4) ) then
            chooser = 'k'
    elseif ( boundaries(2).le.x .and. x.lt.1.0D0         ) then
            chooser = 's'
    else
            call exit(2)       ! shouldn't get here
    endif

    deallocate( boundaries )
  end function chooser
 end module subs

!
!  main program
!
 program rps
  use subs
  implicit none
  real (kind=8) :: past(9)      ! empty hook, at present
  integer       :: score(3)

  call seeder()

  score = (/ 0, 0, 0 /)

  write(6,*) ''
  write(6,*) ' Ro-sham-bo !! '
  call cli( past, score )

  stop
 end program rps
