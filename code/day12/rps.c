#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<stdbool.h>

const char poss[] = "rpszkq";     // array of legitimate enteries

//  use urandom to populate the seed 
void seeder ( void ) {
 unsigned int nseed;
 FILE *blah = fopen( "/dev/urandom", "rb" );
 fread( &nseed, sizeof( nseed ), 1, blah );
 fclose(blah);
 srand( nseed );
 return;
}

void scoreboard ( int score[], bool quit ) {
 if ( quit ) {
   char final[48];
   sprintf( final, "   final score ....... you: %d/%d/%d :cpu",
		 score[0], score[1], score[2] );
   printf( "%s\n\n", final );
   FILE* save = fopen( "score.txt", "w" );
   fprintf( save, "%s\n", final );
   fclose( save );
 } else {
   printf( " current score ....... you: %3d/%3d/%3d :cpu\n",
		 score[0], score[1], score[2] );
 }
 return;
}

void strategy ( const char past[], double bounds[] ) {
  //size_t n = sizeof( bounds ) / sizeof ( bounds[0] );
  int n = 4;
  for( int i=0; i<n; i++ ) {
    bounds[i] =(double) ( i + 1.0 ) / ( n + 1.0 );
  }
  return;
}

char chooser ( const char past[] ) {
  double x, bounds[4]={0.0};
  x =(double) rand() / (RAND_MAX + 1.0);
  strategy( past, bounds );
  if        ( x >= 0.0        &&  x < bounds[0] ) { return 'r';
  } else if ( x >= bounds[0]  &&  x < bounds[1] ) { return 'p';
  } else if ( x >= bounds[1]  &&  x < bounds[2] ) { return 's';
  } else if ( x >= bounds[2]  &&  x < bounds[3] ) { return 'z';
  } else { return 'k'; }
}

char* words ( const char a ) {
  switch (a) {
    case 'r': return "rock";     break;
    case 'p': return "paper";    break;
    case 's': return "scissors"; break;
    case 'z': return "lizard";   break;
    case 'k': return "Spock";    break;
    default:  return "PROBLEM";  break;
  }
}

void scorer ( char opp, char cpu, int score[] ) {
  printf( "  you throw: %s;  cpu throws: %s\n", words(opp), words(cpu) );
  if ( opp == cpu ) {
	  printf( "  this round is a draw!\n" );
	  score[1] = score[1] + 1;
  } else if ( ( opp == 'r' && ( cpu == 's' || cpu == 'z' ) ) ||
              ( opp == 'p' && ( cpu == 'r' || cpu == 'k' ) ) ||
              ( opp == 's' && ( cpu == 'p' || cpu == 'z' ) ) ||
              ( opp == 'z' && ( cpu == 'p' || cpu == 'k' ) ) ||
              ( opp == 'k' && ( cpu == 's' || cpu == 'r' ) ) ) {
	  printf( "  you win this round!\n" );
	  score[0] = score[0] + 1;
  } else {
	  printf( "  you lose this round!\n" );
	  score[2] = score[2] + 1;
  }
  return;
}

void prompt( void ) {
 printf( "  (r)ock, (p)aper, (s)cissors, li(z)ard, Spoc(k) ... or (q)uit\n" );
 printf( " >Ro-sham-bo> " );
 return;
}

int cli ( int score[], char past[] ) {
 int n = 33;
 char arg[n], s='\0', cpu='\0';

 while( 1 ) {   // gameplay loop
   begin: printf("\n");
   scoreboard( score, false );
   prompt();
   fgets( arg, n, stdin );
   strncpy( &s, arg, 1 );

   if ( strchr( poss, s ) == NULL ) { goto begin; }

   if ( s == 'q' ) {
     printf( "\n farewell!\n\n" );
     scoreboard( score, true );
     break;
   } else {
     cpu = chooser( past );  // perhaps put this before the `strsch` to avoid rng manipulation?
     scorer( s, cpu, score );
   }
 }  // end of while loop --- break out with `q` at the prompt

 return 0;
}

//
//  main program
//
int main ( void ) {
 int e = 1;
 int score[3] = {0};
 char past[] = "rpprzkr";  // fake history ... strategy not yet implemented

 seeder();

 printf( "\n Ro-sham-bo !!\n" );
 e = cli( score, past );
 
 return e;
}
